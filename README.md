# Step-forkio

Студент №1: Sabina Bariyeva
Студент №2: Hannochenko Viktor
npm i - Завантаження пакетів для Gulp
npm run dev - запуск в режимі розробника
npm run build - запуск в режимі продакшен
Використовуємі технології:

html;
CSS;
-SCSS;
методології BEM;
JS;
Gulp;
-Figma;
Список використовуємих пакетів:
"browser-sync": "^2.27.10",
"del": "^6.1.1",
"gulp": "^4.0.0",
"gulp-autoprefixer": "^8.0.0",
"gulp-clean": "^0.4.0",
"gulp-clean-css": "^4.3.0",
"gulp-concat": "^2.6.1",
"gulp-debug": "^4.0.0",
"gulp-file-include": "^2.3.0",
"gulp-imagemin": "^8.0.0",
"gulp-minify": "^3.1.0",
"gulp-notify": "^4.0.0",
"gulp-plumber": "^1.2.1",
"gulp-rename": "^2.0.0",
"gulp-sass": "^5.1.0",
"gulp-sourcemaps": "^3.0.0",
"gulp-svg-sprite": "^1.5.0",
"gulp-uglify-es": "^3.0.0",
"sass": "^1.54.4".